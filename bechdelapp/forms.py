from django import forms
from .models import Movie, Comment, Rating

class MovieForm(forms.ModelForm):

    class Meta:
        model = Movie
        fields = ('title', 'language', 'year', 'wikipedia_link',)
        labels = {'wikipedia_link': 'Wikipedia Link'}

class CommentForm(forms.ModelForm):

    class Meta:
        model = Comment
        fields = ('author', 'text',)
        labels = {'text':'Comment'}

class RatingForm(forms.ModelForm):

    class Meta:
        model = Rating
        fields = ('author', 'criterion_no_two_women', 'criterion_two_women', 'criterion_speak_each_other', 'criterion_anything_but_men')
        labels = {'criterion_no_two_women':'There are not even two women in the movie','criterion_two_women':'There are two named women characters in the movie','criterion_speak_each_other':'Who speak with each other','criterion_anything_but_men':'About anything other than men'}
