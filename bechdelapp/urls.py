from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('about/', views.about, name='about'),
    path('download/', views.download, name='download'),
    path('movies/', views.movie_list, name='movie_list'),
    path('movie/<int:pk>/', views.movie_detail, name='movie_detail'),
    path('movie/new', views.movie_new, name='movie_new'),
    path('movie/<int:pk>/comment/$', views.add_comment_to_movie, name='add_comment_to_movie'),
    path('movie/<int:pk>/rate/$', views.rate_movie, name='rate_movie'),
    path('movies/ta', views.movie_list_ta, name='movie_list_ta'),
    path('movies/te', views.movie_list_te, name='movie_list_te'),
    path('movies/hi', views.movie_list_hi, name='movie_list_hi'),
    path('movies/ml', views.movie_list_ml, name='movie_list_ml'),
    path('movies/mr', views.movie_list_mr, name='movie_list_mr'),
    path('movies/bn', views.movie_list_bn, name='movie_list_bn'),
    path('movies/kn', views.movie_list_kn, name='movie_list_kn'),
    path('movies/pn', views.movie_list_pn, name='movie_list_pn'),
]
