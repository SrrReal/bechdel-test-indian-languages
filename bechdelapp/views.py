from django.shortcuts import render, get_list_or_404, get_object_or_404, redirect
from django.http import HttpResponse
from .models import Movie, Rating
from .forms import MovieForm, CommentForm, RatingForm
from djqscsv import write_csv

def index(request):
    return render(request, 'bechdelapp/index.html')
#    return HttpResponse("Hello, world. Welcome to Bechdel test for Indian language movies.")

def movie_list(request):
    movies = Movie.objects.all()
    return render(request, 'bechdelapp/movie_list.html', {'movies':movies})

def movie_list_ta(request):
    movies = Movie.objects.filter(language='TA')
    language = 'Tamil'
    return render(request, 'bechdelapp/movie_list.html', {'movies':movies, 'language':language})

def movie_list_te(request):
    movies = Movie.objects.filter(language='TE')
    language = 'Telugu'
    return render(request, 'bechdelapp/movie_list.html', {'movies':movies, 'language':language})

def movie_list_hi(request):
    movies = Movie.objects.filter(language='HI')
    language = 'Hindi'
    return render(request, 'bechdelapp/movie_list.html', {'movies':movies, 'language':language})

def movie_list_bn(request):
    movies = Movie.objects.filter(language='BN')
    language = 'Bengali'
    return render(request, 'bechdelapp/movie_list.html', {'movies':movies, 'language':language})

def movie_list_kn(request):
    movies = Movie.objects.filter(language='KN')
    language = 'Kannada'
    return render(request, 'bechdelapp/movie_list.html', {'movies':movies, 'language':language})

def movie_list_ml(request):
    movies = Movie.objects.filter(language='ML')
    language = 'Malayalam'
    return render(request, 'bechdelapp/movie_list.html', {'movies':movies, 'language':language})

def movie_list_mr(request):
    movies = Movie.objects.filter(language='MR')
    language = 'Marathi'
    return render(request, 'bechdelapp/movie_list.html', {'movies':movies, 'language':language})

def movie_list_pn(request):
    movies = Movie.objects.filter(language='PN')
    language = 'Punjabi'
    return render(request, 'bechdelapp/movie_list.html', {'movies':movies, 'language':language})

def movie_detail(request, pk):
    movie = get_object_or_404(Movie, pk=pk)
    #ratings = get_list_or_404(Rating, movie=pk)
    score_0 = Rating.objects.filter(movie__title__contains=movie.title).filter(score=0).count()
    score_1 = Rating.objects.filter(movie__title__contains=movie.title).filter(score=1).count()
    score_2 = Rating.objects.filter(movie__title__contains=movie.title).filter(score=2).count()
    score_3 = Rating.objects.filter(movie__title__contains=movie.title).filter(score=3).count()
    total_scorers = Rating.objects.filter(movie__title__contains=movie.title).count()
    if (total_scorers != 0):
        score_0 = (score_0/total_scorers)*100
        score_1 = (score_1/total_scorers)*100
        score_2 = (score_2/total_scorers)*100
        score_3 = (score_3/total_scorers)*100
    score_list = [score_0, score_1, score_2, score_3, total_scorers]
    return render(request, 'bechdelapp/movie_detail.html', {'movie':movie, 'score_list':score_list})

def movie_new(request):
    if request.method == "POST":
        form = MovieForm(request.POST)
        if form.is_valid():
            movie = form.save(commit=False)
            movie.save()
            return redirect('movie_detail', pk=movie.pk)
    else:
        form = MovieForm()
        return render(request, 'bechdelapp/movie_edit.html', {'form':form})

def add_comment_to_movie(request, pk):
    movie = get_object_or_404(Movie, pk=pk)
    if request.method == "POST":
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.movie = movie
            comment.save()
            return redirect('movie_detail', pk=movie.pk)
    else:
        form = CommentForm()
    return render(request, 'bechdelapp/add_comment_to_movie.html', {'form':form,'movie':movie})

def rate_movie(request, pk):
    movie = get_object_or_404(Movie, pk=pk)
    if request.method == "POST":
        form = RatingForm(request.POST)
        if form.is_valid():
            rating = form.save(commit=False)
            rating.movie = movie
            rating.score = rating.calculate_score()
            rating.save()
            return redirect('movie_detail', pk=movie.pk)
    else:
        form = RatingForm()
    return render(request, 'bechdelapp/rate_movie.html',{'form':form,'movie':movie})

def download(request):
    qs = Rating.objects.values('movie__title', 'movie__year', 'score')
    with open('fool.csv', 'wb') as csv_file:
        write_csv(qs, csv_file)
    return render(request, 'bechdelapp/download.html')

def about(request):
    return render(request, 'bechdelapp/about.html')
