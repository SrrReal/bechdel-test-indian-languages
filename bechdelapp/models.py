from django.db import models
from django.utils import timezone

# Create your models here.
class Movie(models.Model):
    title = models.CharField(max_length=500)
    TAMIL = 'TA'
    TELUGU = 'TE'
    HINDI = 'HI'
    MALAYALAM = 'ML'
    BENGALI = 'BN'
    KANNADA = 'KN'
    MARATHI = 'MR'
    PUNJABI = 'PN'
    LANGUAGE_CHOICES = (
        (TAMIL, 'Tamil'),
        (TELUGU, 'Telugu'),
        (HINDI, 'Hindi'),
        (MALAYALAM, 'Malayalam'),
        (BENGALI, 'Bengali'),
        (KANNADA, 'Kannada'),
        (MARATHI, 'Marathi'),
        (PUNJABI, 'Punjabi'),
    )
    language = models.CharField(max_length=2,choices=LANGUAGE_CHOICES)
    year = models.PositiveSmallIntegerField()
    wikipedia_link = models.URLField()

    def __str__(self):
        return self.title

class Comment(models.Model):
    movie = models.ForeignKey('bechdelapp.Movie', on_delete=models.CASCADE, related_name='comments')
    author = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    approved_comment = models.BooleanField(default=False)

    def approve(self):
        self.approved_comment = True
        self.save()

    def __str__(self):
        return self.text

class Rating(models.Model):
    movie = models.ForeignKey('bechdelapp.Movie', on_delete=models.CASCADE, related_name='ratings')
    author = models.CharField(max_length=200)
    criterion_no_two_women = models.BooleanField(default=False)
    criterion_two_women = models.BooleanField(default=False)
    criterion_speak_each_other = models.BooleanField(default=False)
    criterion_anything_but_men = models.BooleanField(default=False)
    score = models.PositiveSmallIntegerField(null=True)

    def calculate_score(self):
        if self.criterion_no_two_women == True:
            return 0
        else:

            if self.criterion_two_women == False:
                two_women_score = 0
            else:
                two_women_score = 1

            if self.criterion_speak_each_other == False:
                speak_each_other_score = 0
            else:
                speak_each_other_score = 1

            if self.criterion_anything_but_men == False:
                anything_but_men_score = 0
            else:
                anything_but_men_score = 1

            return two_women_score + speak_each_other_score + anything_but_men_score

    def __str__(self):
        return self.title
